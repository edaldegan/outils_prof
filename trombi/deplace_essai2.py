from tkinter import *
from PIL import Image, ImageTk
from random import shuffle
import io
 
# https://python.developpez.com/cours/TutoSwinnen/?page=page_16#L14-3

class Draw():
    """classe définissant la fenêtre principale du programme"""
    def __init__(self):
        # mise en place du canevas nommé c
        self.fenetre = Tk()
        fond = PhotoImage(file="plan.png")
        #fond = ImageTk.PhotoImage(Image.open("plan.png")) # image de fond
        self.c = Canvas(self.fenetre, width=1500, height=800, bg='lightgrey')
        self.c.create_image(0, 0, anchor=NW, state=DISABLED, image=fond)
        
        # collage des photos
        photosTk = []    # liste photos tk
        for i in range(1, 5):
            temp = Image.open("eleve" + str(i) + ".png")   # image PIL
            #temp.resize(20, 0)
            photosTk.append(ImageTk.PhotoImage(temp))    # image tk
        shuffle(photosTk)
        
        for i in range(len(photosTk)):   # collage
            self.c.create_image(50 + i*300, 200, anchor=NW, image=photosTk[i])
            self.c.pack(padx=5, pady=3)
        
        # liaison d'événements <souris> au widget <canevas>
        self.c.bind("<Button-1>", self.mouseDown)
        self.c.bind("<Button1-Motion>", self.mouseMove)
        self.c.bind("<Button1-ButtonRelease>", self.mouseUp)
        
        # mise en place d'un bouton de sortie
        b_fin = Button(self.fenetre, text='Terminer', bg ='royal blue', fg='white',font=('Helvetica', 10, 'bold'), command=quit)
        b_fin.pack(pady=2)
        # mise en place d'un bouton de sauvegarde
        b_save = Button(self.fenetre, text='Valider Plan', bg ='royal blue', fg='white',font=('Helvetica', 10, 'bold'), command=self.saveFig)
        b_save.pack(pady=2)
            
        self.fenetre.mainloop()
    
    def saveFig(self):
        """ Méthode de sauvegarde à invoquer à la commande du bouton """
        ps = self.c.postscript(colormode='color')
        img = Image.open(io.BytesIO(ps.encode('utf-8')))
        img.save('filename.jpg', 'jpeg')
        print(" plan sauvegardé !")
        
        
    def mouseDown(self, event):
        """Op. à effectuer quand le bouton gauche de la souris est enfoncé"""
        self.currObject = None
            # event.x et event.y contiennent les coordonnées du clic effectué :
        self.x1, self.y1 = event.x, event.y
            # <find_closest> renvoie la référence du dessin le plus proche :
        self.selObject = self.c.find_closest(self.x1, self.y1)
            # modification de l'épaisseur du contour du dessin :
        self.c.itemconfig(self.selObject)
            # <lift> fait passer le dessin à l'avant-plan :
        self.c.lift(self.selObject)
 
    def mouseMove(self, event):
        """Op. à effectuer quand la souris se déplace, bouton gauche enfoncé"""
        x2, y2 = event.x, event.y
        dx, dy = x2 - self.x1, y2 - self.y1
        if self.selObject:
            self.c.move(self.selObject, dx, dy)
            self.x1, self.y1 = x2, y2
 
    def mouseUp(self, event):
        """Op. à effectuer quand le bouton gauche de la souris est relâché"""
        if self.selObject:
            self.c.itemconfig(self.selObject)
            self.selObject = None


fenetre = Draw()
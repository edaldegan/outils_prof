from PIL import Image, ImageTk
from random import shuffle
from tkinter import *


racine = Tk()
# les labels

#labelValider = Label(racine, text="") # label de la chaîne de validation
#labelValider.grid(row=4, column=1, columnspan=2, sticky="W",padx=10)

# Liste des vignettes
photos = []
for i in range(1, 5):
    photos.append(Image.open("eleve" + str(i) + ".png"))
shuffle(photos)

# les entrées
#entree1 = Entry(racine)
#entree1.grid(row=1, column=2)

# le cannevas et son image de fond (1200x864)
cannevasImg = Canvas(racine, width=1200, height=864, bg="pink")
cannevasImg.pack()

"""
for i in range(1, 5):
    #img = ImageTk.PhotoImage(Image.open(photos[i])
    #panel = Label(racine, image=img)
    #panel.grid(row=4, column=i, rowspan=4, padx=10, pady=10)
    photo = PhotoImage(file=photos[i])
    image = cannevasImg.create_image(0, 0, image=photo)
    image.grid(row=4, column=i, rowspan=4, padx=10, pady=10)
"""

# les boutons et leur gestion
def valider () :
    chaine = entree1.get()+" // "+ entree2.get()+ " // "+ entree3.get()
    labelValider.config(text=chaine)
def initialiser ():
    labelValider.config(text="")
    entree3.delete (0, END)
    entree2.delete (0, END)
    entree3.delete (0, END)
    entree1.focus_set()
    
boutonQuitter = Button(racine, text="Quitter", command=racine.destroy)
boutonQuitter.grid(row=5, column=3, pady=10)
boutonValider = Button(racine, text="Valider", command=valider)
boutonValider.grid(row=5, column=1, pady=10)
boutonInitialiser = Button(racine, text="Réinitialiser", command=initialiser)
boutonInitialiser.grid(row=5, column=2, pady=10)

racine.mainloop()
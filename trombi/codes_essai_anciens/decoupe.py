
def decoupe_trombi(fichier, nb_eleves):
    """ découpe d'un fichier trombinoscope standard (< 42 élèves) pour avoir
        des vignettes numerotées au format  108x146
    """
    from PIL import Image

    img = Image.open(fichier)
    compteur = 0

    for i in range(0, 7):  # sur une colonne
        ya = 150 + i*215
        yb = ya + 195
        xa = 71
        xb = xa + 145    # vignette de 108x146
        for j in range(0, 6):  #sur une ligne
            box = (xa , ya, xb, yb)
            decoupe = img.crop(box)
            decoupe = decoupe.resize((108, 146))

            if compteur < nb_eleves:
                compteur = compteur + 1
                decoupe.save("eleve" + str(compteur) + ".png" )
            xa = xa + 191
            xb = xa + 140

decoupe_trombi("1spePC.jpg", 37)

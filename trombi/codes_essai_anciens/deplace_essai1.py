

"""  A AMELIORER :

    filepath = tk.askopenfilename(title="Ouvrir une image", filetypes=[('png files','.png'),('all files','.*')])
    photo = PhotoImage(file=filepath)

    """

from PIL import Image, ImageTk
import tkinter as tk
from tkinter import PhotoImage, Canvas


def clic(event):
    """ Gestion de l'événement Clic gauche """
    global detect_clic
    global vignette
    global nombre
    vignette = 1

    x = event.x     # position du pointeur de la souris
    y = event.y

    # coordonnées de l'objet
    while vignette <= nombre:
        [x_min, y_min] = Canevas.coords(vignette)  #Position objet  x_min, y_min

        if x_min <= x <= x_min + LargeurI and y_min <= y < y_min + HauteurI:
            detect_clic = True   # Si clic x, y sur la photo
            break
        else:
            detect_clic = False
        vignette = vignette + 1


def drag(event):
    """ Gestion de l'événement bouton gauche enfoncé """

    x = event.x + (LargeurI / 2)  # position du pointeur de la souris x, y
    y = event.y + (HauteurI / 2)

    if detect_clic:
        # limite de l'objet dans la zone graphique
        if x < LargeurI:
            x = LargeurI
        if x > Largeur:
            x = Largeur
        if y < HauteurI:
            y = HauteurI
        if y > Hauteur:
            y = Hauteur

        # mise à jour de la position de l'objet (drag)
        Canevas.coords(vignette, x - LargeurI, y - HauteurI)


# Création de la fenêtre principale
plan_de_classe = tk.Tk()
plan_de_classe.title("Plan de classe")


# CREATION DU CANVAS
# Image de fond
fond1 =Image.open("plan.png")   # image PIL
fond = ImageTk.PhotoImage(fond1) # image Tk

# l x h du canevas aux dimension de l'image de fond
Largeur = fond1.size[0]
Hauteur = fond1.size[1]

Canevas = tk.Canvas(plan_de_classe, width=Largeur, height=Hauteur, bg="white")
Canevas.create_image(0, 0, anchor=tk.NW, state=tk.DISABLED, image=fond)

# Voir https://anzeljg.github.io/rin2/book2/2405/docs/tkinter/create_image.html

# CREATION DES OBJETS A COLLER
# Liste des vignettes format lxh  108x146
HauteurI = 146  # hauteur de la vignette
LargeurI = 108  # largeur de la vignette
photos = []
photosTk = []
for i in range(1, 4):
    temp = Image.open("eleve" + str(i) + ".png")   # image PIL
    photos.append(temp)
    photosTk.append(ImageTk.PhotoImage(temp))    # image tk


image =[]
for i in range(len(photosTk)):
    image.append(Canevas.create_image(0 + i*45, 0, anchor=tk.NW, image=photosTk[i]))


# Initialisation des variables globales
vignette = 1
detect_clic = False
nombre = len(photosTk)

# La méthode bind() permet de lier un événement avec une fonction
Canevas.bind('<Button-1>', clic)  # événement clic gauche (press)
Canevas.bind('<B1-Motion>', drag)  # événement bouton gauche enfoncé (hold down)


Canevas.focus_set()
Canevas.pack(padx=10, pady=10)
plan_de_classe.mainloop()

from PIL import Image
from random import shuffle

# Définition des coordonnées de collage des photos
coord = []
for i in range(8):
    for j in range(5):
        coord.append((20 + i*108, 20 + j*150))

# Liste des vignettes
photos = []
for i in range(1, 5):
    photos.append(Image.open("eleve" + str(i) + ".png"))
shuffle(photos)

# collage sur le fond
plan = Image.open("plan.png")
for i in range(len(photos)):
    plan.paste(photos[i], coord[i])

plan.save("plan_final1.png")

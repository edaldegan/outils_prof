import pygame
from random import shuffle

surf = pygame.display.set_mode((1200,864))   # dimensions du plan, image de fond
run = True
N = 4  # nombre d'élèves

# image de fonc
img = pygame.image.load("plan.png")

# images d'élèves
eleves = []
for i in range(1, N):
    fichier = "eleve" + str(i) + ".png"
    #print(eleve)
    eleve = pygame.image.load(fichier)
    largeur = eleve.get_rect().width
    hauteur = eleve.get_rect().height    
    eleve_reduit = pygame.transform.scale(eleve, (largeur//2, hauteur//2))
    eleves.append(eleve_reduit)
shuffle(eleves)

while run :
  for event in pygame.event.get():
    if event.type == pygame.QUIT:    # Arret de l'appli
      run = False
    if event.type == pygame.MOUSEBUTTONDOWN :    # click gauche
      if pygame.mouse.get_pressed() == (1,0,0) :
        pos = pygame.mouse.get_pos()
        print(pos)
  surf.fill((0,0,0))   # couleur de fond
  surf.blit(img,(0,0))  # colle image de fond
  
  for i in range(N-1):
      surf.blit(eleves[i],(100 + i*200,100))  # colle eleve
    
        
  pygame.display.flip()
pygame.quit()